
class Domain < ActiveRecord::Base
	belongs_to :account

	validates  :hostname, :uniqueness => true,
						:presence => true		
	
	scope :search, lambda {|query|
		where(["name LIKE ?", "%#{query}%"])
	}

	def look_up_ip(host, id)
	    
	    
	    @domain = Domain.find(id)
	    @domain.origin_ip = Resolv.getaddress(host)  
	    @domain.save
	end

	
end
