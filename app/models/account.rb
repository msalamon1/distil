class Account < ActiveRecord::Base
	has_many :domains

	validates  :name, :uniqueness => true,
						:presence => true		
	
	scope :search, lambda {|query|
		where(["name LIKE ?", "%#{query}%"])
	}
end
