class DomainsController < ApplicationController


  def index

    @domains = Domain.all

  end

  def show
    @domain = Domain.find(params[:id])
  end

  def new

    @domain = Domain.new({:hostname => "Enter hostname here"})
    @accounts = Account.all
    @domain_count = Domain.count +1
    

  end

  def edit
    @domain = Domain.find(params[:id])
    @accounts = Account.all
    @domain_count = Domain.count
  end

   def delete
    @domain = Domain.find(params[:id])

  end

  def destroy
     domain = Domain.find(params[:id]).destroy
     flash[:notice] = "domain '#{domain.hostname}' destroyed successfully."
     redirect_to(:action => 'index')

  end


def create
    # Instantiate a new object using form parameters
    @domain = Domain.new(domain_params)
    #@domain.origin_ip = Resolv.getaddress(@domain.hostname)
    
    # Save the object
    if @domain.save
    @domain.delay.look_up_ip(@domain.hostname, @domain.id)
    # If save succeeds, redirect to the index action
    flash[:notice] = "Domain created successfully."

    redirect_to(:action => 'index')
    else
      # If save fails, redisplay the form so user can fix problems
    @accounts = Account.all
    @domain_count = Domain.count +1
    render('new')
    end
  end

def update
      # Find an existing object using form parameters
      @domain = Domain.find(params[:id])
      
      # update the object
      if @domain.update_attributes(domain_params)
      # If update succeeds, redirect to the index action
      flash[:notice] = "Domain updated successfully."
      redirect_to(:action => 'show', :id => @domain.id)
      else
        # If update fails, redisplay the form so user can fix problems
       
      @accounts = Account.all 
      render('edit')
      end

    end




private
    def domain_params 
      params.require(:domain).permit(:hostname, :account_id, :origin_ip, :created_at, :updated_at)
    end
end
