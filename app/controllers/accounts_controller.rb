class AccountsController < ApplicationController
 

  def index
    @accounts = Account.all
  end

  def show
    @account = Account.find(params[:id])
  end

  def new
    @account = Account.new
  end

  def create
    # Instantiate a new object using form parameters
    @account = Account.new(account_params)
    # Save the object
    if @account.save
    # If save succeeds, redirect to the index action
    flash[:notice] = "Account created successfully."
    redirect_to(:action => 'index')
    else
      # If save fails, redisplay the form so user can fix problems
     flash[:notice] = "Account name must be unique"
     @account_count = Account.count + 1
     render('new')
     
    end

  end
def edit
    @account = Account.find(params[:id])
    @account_count = Account.count
  end

  def update
      # Find an existing object using form parameters
      @account = Account.find(params[:id])
      
      # update the object
      if @account.update_attributes(account_params)
      # If update succeeds, redirect to the index action
      flash[:notice] = "Account updated successfully."
      redirect_to(:action => 'show', :id => @account.id)
      else
        # If update fails, redisplay the form so user can fix problems
       @account_count = Account.count
       render('edit')
       
      end

    end



  def delete
    @account = Account.find(params[:id])
    
  end

  def destroy
     account = Account.find(params[:id]).destroy
     flash[:notice] = "Account '#{account.name}' destroyed successfully."
     redirect_to(:action => 'index')

  end


  private
    def account_params 
      #whitelist of strong params
      params.require(:account).permit(:name, :created_at, :updated_at)
    end
end
