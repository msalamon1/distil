class CreateDomains < ActiveRecord::Migration
  	def up
    create_table :domains do |t|
    	t.integer "account_id"
    	t.string "hostname"
    	t.string "origin_ip"
      t.timestamps
    end
    add_index("domains", "account_id")
  end
  	def down
  drop_table :domains
	end
end
